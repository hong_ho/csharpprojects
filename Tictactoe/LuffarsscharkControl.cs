﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termin_3___Labb2
{
    class LuffarscharkControl
    {
        private LuffarscharkModel M;

        public LuffarscharkControl(LuffarscharkModel m)
        {
            M = m;
        }
        public void spela(int i)
        {
            M.play(i);
        }
        public void exit()
        {
            M.stop();
        }
        public void reset()
        {
            M.reset();
        }
       
    }
}
