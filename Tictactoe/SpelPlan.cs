﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Termin_3___Labb2
{
    public partial class SpelPlan : Form, IObserver
    {
        private LuffarscharkControl C;
        private LuffarscharkModel M;
        private Button[] knapp=new Button[9];
        public SpelPlan()
        {
            M = new LuffarscharkModel();
            C = new LuffarscharkControl(M);
           
            M.addObserver(this);
            InitializeComponent();
            exit_B.Enabled = false;
            reset_B.Enabled = false;
            textBox2.Enabled = false;

            for (int i = 0; i < 9; i++)
            {
                knapp[i] = new Button();
                knapp[i].Width = 50;
                knapp[i].Height = 30;
                knapp[i].ForeColor = Color.White;
                knapp[i].Font = new Font("Arial", 25);
                knapp[i].Dock = DockStyle.Fill;
                knapp[i].BackColor = Color.Purple;
                spelPlanPanel.Controls.Add(knapp[i]);
                knapp[i].Tag = i;
                knapp[i].Click += SpelPlan_Click;
                knapp[i].Enabled = false;

            }

        }

        private void exit_B_click(object sender, System.EventArgs e)
        {
            C.exit();
           // throw new System.NotImplementedException();
        }

        //private void start_B_click(object sender, System.EventArgs e)
        //{
        //    //for (int i = 0; i < 9; i++)
        //    //{
        //    //    knapp[i] = new Button();
        //    //    knapp[i].Width = 50;
        //    //    knapp[i].Height = 30;
        //    //    knapp[i].ForeColor = Color.White;
        //    //    knapp[i].Font= new Font("Arial",25);
        //    //    knapp[i].Dock = DockStyle.Fill;
        //    //    knapp[i].BackColor = Color.Purple;
        //    //    spelPlanPanel.Controls.Add(knapp[i]);
        //    //    knapp[i].Tag = i;
        //    //    knapp[i].Click += SpelPlan_Click;

        //    //}
        //    //start_B.Enabled = false;
            
        //}

        void SpelPlan_Click(object sender, EventArgs e)
        {
            C.spela((int)((Button)sender).Tag);
           
          
           
             //   MessageBox.Show(knapp[i].Text=i.ToString());
            
        }


        public void update(ILObservable io)
        {
            LuffarscharkModel M = (LuffarscharkModel)io;

            for (int i = 0; i < 9; i++)
            {
                if (!(M.getGameBoard()[i] == 0))
                {
                    if (M.getGameBoard()[i] == 1)
                        this.knapp[i].Text = ("X");
                    else
                        this.knapp[i].Text = ("O");

                }
            }

            if (M.vinn() > 0)
            {
                if (M.getspelare() == 1)
                {
                    MessageBox.Show(textBox1.Text + " vinner. Grattis! :-)");
                    for (int i = 0; i < 9; i++)
                    {
                        knapp[i].Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show(textBox2.Text + " vinner. Grattis! :-)");
                    for (int i = 0; i < 9; i++)
                    {
                        knapp[i].Enabled = false;
                    }
                }
            }
            else if (M.checkFull())
            {
                MessageBox.Show( " Spelplanen är uppfylld. ");  
            }
           if (!M.isPlaying())
            {
                textBox1.Text = "";
                textBox1.Enabled = true;
                textBox2.Text = "";
                textBox2.Enabled = false;
            //    start_B.Enabled = false;
                for (int i = 0; i < 9; i++)
                {
                    knapp[i].Text = "";
                    knapp[i].Enabled = false;

                }
                M.setPlaying(true);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
               textBox2.Enabled=true;
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //start_B.Enabled = true;
            //start_B.ForeColor = Color.White;
            exit_B.Enabled = true;
            reset_B.Enabled = true;


            for (int i = 0; i < 9; i++)
            {
               
                knapp[i].Enabled=true;

            }
            
        }

        private void reset_B_Click(object sender, EventArgs e)
        {
            C.reset();
            
        }

      
        
    }
}
