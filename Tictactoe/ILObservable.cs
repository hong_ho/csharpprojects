﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termin_3___Labb2
{
    public interface ILObservable
    {
        void addObserver(IObserver obs);
        void setChanged();
        void notifyObservers();
    }
}
