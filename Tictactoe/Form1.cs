﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Termin_3___Labb2
{
    public partial class Form1 : Form
    {
        Timer progressTimer;
        public Form1()
        {
            InitializeComponent();
            progressTimer = new Timer();
            progressTimer.Interval = 10; // 100 thi chay nhanh hon
            progressTimer.Tick += progressTimer_Tick; // tang len tung buoc
        }

        void progressTimer_Tick(object sender, EventArgs e)
        {
            progressBar1.Value++; // tang len
            if (progressBar1.Value == 100)
            {
                
                this.Hide();
                progressTimer.Stop();
                SpelPlan f2 = new SpelPlan();
                f2.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressTimer.Start();
           

        }
       
    }
}
