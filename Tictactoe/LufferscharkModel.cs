﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termin_3___Labb2
{
   public class LuffarscharkModel : ILObservable
    {
        private List<IObserver> observers;
        private int _spelare;
        private int[] gameBoard = new int[9];
        private int win;
        private bool playing;


        public LuffarscharkModel()
        {
            this._spelare = 1;
            for(int i=0; i<gameBoard.Length;i++)
                gameBoard[i] = 0;
            observers = new List<IObserver>();
           
        }
        public int getspelare()
        {     return _spelare;        }
        public void setSpelare(int value)
        {    _spelare=value;         }
        public int[] getGameBoard()
        { return gameBoard; }
        public void setGameBoard(int[] gb)
        { gameBoard = gb; }
        public void checkTur()
        {
            if (this._spelare == 1)
                this._spelare = 2;
            else
                this._spelare = 1;
        }
        public void play(int pos)
        {
            if (gameBoard[pos] == 0)
            {
                playing = true;
                gameBoard[pos]=this.getspelare();
                checkWin(this._spelare);
                checkFull();            
                notifyObservers();
                checkTur();
            }
        }

        public int vinn()
        {
            return win;
        }
        public void checkWin(int spelare)
        {
            if ((gameBoard[0] == spelare && gameBoard[1] == spelare && gameBoard[2] == spelare) ||
                (gameBoard[3] == spelare && gameBoard[4] == spelare && gameBoard[5] == spelare) ||
                (gameBoard[6] == spelare && gameBoard[7] == spelare && gameBoard[8] == spelare) ||
                (gameBoard[0] == spelare && gameBoard[3] == spelare && gameBoard[6] == spelare) ||
                (gameBoard[1] == spelare && gameBoard[4] == spelare && gameBoard[7] == spelare) ||
                (gameBoard[2] == spelare && gameBoard[5] == spelare && gameBoard[8] == spelare) ||
                (gameBoard[0] == spelare && gameBoard[4] == spelare && gameBoard[8] == spelare) ||
                (gameBoard[6] == spelare && gameBoard[4] == spelare && gameBoard[2] == spelare))
                win = spelare;
                
        }
        public bool isPlaying()
        {
            return playing;
        }
        public void setPlaying(bool b)
        {
            playing = true;

        }
        public bool checkFull()
        {
            for (int i = 0; i <9; i++)
                if (gameBoard[i] == 0)
                    return false;
            return true;
        }
        public void reset()
        {
            playing = false;
            for (int i = 0; i < gameBoard.Length; i++)
            { gameBoard[i] = 0; }
            win = 0;
            this._spelare = 1;

            this.setChanged();
            this.notifyObservers();
        }

        public void stop()
        {
            playing = false;
            System.Environment.Exit(0);
            
        }
        public void addObserver(IObserver obs)
        {
            observers.Add(obs);
        }
        public void setChanged()
        {
        }
        public void notifyObservers()
        {
            foreach (IObserver obs in observers)
                obs.update(this);
        }
    }
}
