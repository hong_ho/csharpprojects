﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic;
using System.Xml;
namespace SystemAffar
{
    public partial class Lagerarbete : Form
    {
        List<Produkt> listpro = new List<Produkt>();
        private StreamReader sr;
        private StreamWriter sw;
        private string filNamn = "produkt.txt";
        public Lagerarbete()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            checkInMata();
        }
        /*
         * Testa unika inmatning och spara till filen
         * */
        private void checkInMata()
        {
            bool test = false;
            FileInfo info = new FileInfo(filNamn);
            if(info.Length==0)
            {
                skrivIn();
                test = true;

            }
            else
            {
                string[] linePro;
                bool flag = false;
                sr = new StreamReader(filNamn);
                while (!sr.EndOfStream && test==false)
                {
                    string line = sr.ReadLine();
                    linePro = line.Split(';');

                    string ids = linePro[0];
                    string name = linePro[1];            
                    
                    if (txtID.Text.Equals(ids))
                    {
                        MessageBox.Show("inte unikt ID");
                        flag = false;
                        break;
                    }
                      else if (txtName.Text.Equals(name))
                      {
                          MessageBox.Show("inte unikt namn");
                          flag = false;
                          break;
                      }
                    else
                    {
                        flag = true;
                    }
                }
                sr.Close();
                if (flag == true)
                {
                    skrivIn();                    
                } 
            }           
        }
        /*
         * Testa rätt inmatningar av en produkt och skriva varje produkt till filen
         * */
        private void skrivIn()
        {
            Produkt p = new Produkt();
            bool isID, isPris, isAntal;
            int id, antal;
            decimal pris;

            isID = int.TryParse(txtID.Text, out id);
            isPris = decimal.TryParse(txtPris.Text, out pris);
            isAntal = int.TryParse(txtAntal.Text, out antal);
            string name = txtName.Text;

            if (isID == true && isAntal == true)
            {
                p.VaruID = id;
                p.Name = name;
                p.Pris = pris;
                p.Antal = antal;
                listpro.Add(p);
                skrivProdukt(p.VaruID, p.Name, p.Pris, p.Antal);
                ClearTextboxes();
            }
            else
                MessageBox.Show("ID:int - Pris:Decimai - Antal:int");
        }
        /*
         * Tömma information från textboxes
         * */
        public void ClearTextboxes()
        {
            foreach (Control tbox in Controls)
            {
                if (tbox is TextBox)
                    ((TextBox)tbox).Clear();
            }
        }
        /*
         * Skriva in varje produkt till filen
         * */
        public void skrivProdukt(int id, string name, decimal pris, int antal)
        {
            sw = new StreamWriter(filNamn, true);
            sw.WriteLine(id + ";" + name + ";" + pris + ";" + antal);
            sw.Close();
        }
      
        private void btnList_Click(object sender, EventArgs e)
        {           
            laddaUppFil();
        }
        /*
         * Ladda upp alla produkter från filen till datagridview
         * */
        private void laddaUppFil()
        {
            sr = new StreamReader(filNamn);
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string[] linePro = line.Split(';');
                Produkt pr = new Produkt();
                bool isID, isPris, isAntal;
                int k, a;
                decimal pris;

                isID = int.TryParse(linePro[0], out k);
                isPris = decimal.TryParse(linePro[2], out pris);
                isAntal = int.TryParse(linePro[3], out a);
                if (isID == true && isPris == true && isAntal==true)
                {
                    pr.VaruID = k;
                    pr.Pris = pris;
                    pr.Antal = a;
                }
                pr.Name = linePro[1];
                listpro.Add(pr);
            }
            sr.Close();

            bindingSource1.DataSource = listpro;
            dataGridView1.DataSource = bindingSource1;
        }
        /*
         * Ta bort en vald rad från datagridview, efterdet sparas information till filen
         * */
        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
            {
                if (oneCell.Selected)
                {
                    int value = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow selectR = dataGridView1.Rows[value];
                    // string va = Convert.ToString(selectR.Cells["antal"].Value);
                    int va = (int)selectR.Cells["antal"].Value;
                    if (va > 0)
                    {
                        DialogResult r = MessageBox.Show("Vill ta bort?", "Delete", MessageBoxButtons.YesNo);
                        if (r == DialogResult.Yes)
                        {                            
                            dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
                        }
                        else
                        {
                            ;
                        }
                    }
                    else if (va == 0 && va < 0)
                    {
                        dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
                    }
                }

            }
            SaveDatagridView();
        }
        /*
         * Spara information från datagridview till filen
         * */
        private void SaveDatagridView()
        {
            sw = new StreamWriter(filNamn);
            for (int x = 0; x < dataGridView1.Rows.Count - 1; x++)
            {
                for (int y = 0; y < dataGridView1.Columns.Count; y++)
                {
                    sw.Write(dataGridView1.Rows[x].Cells[y].Value.ToString());
                    if (y != dataGridView1.Columns.Count - 1)
                    {
                        sw.Write(";");
                    }
                }
                sw.WriteLine();
            }
            sw.Close();
        }
        /*
         * Ändra "Antal" produkt från datagridview, efterdet spara till filen
         * */
        private void btnChange_Click(object sender, EventArgs e)
        {
            string antal = Interaction.InputBox("Ändra antal av en produkt:");
            foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
            {
                if (oneCell.Selected)
                {
                    int value = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow selectR = dataGridView1.Rows[value];
                    selectR.Cells["antal"].Value = antal;
                }
            }
            SaveDatagridView();
        }
        /*
         * Länka till Home GUI
         * */
        private void btnHem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }
        /*
         * Länka till Kassabruk GUI
         * */
        private void btnKassabruk_Click(object sender, EventArgs e)
        {
            Kassabruk k = new Kassabruk();
            k.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            KlassDiagram k = new KlassDiagram();
            k.Show();
            this.Hide();
        }

        private void exporteraProduktregisterToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string source = @"C:\\Hong\\BiZTalk\\TvavagIntegration\\SystemAffar_V2\\SystemAffar\\bin\\Debug\\produkt.txt";
            string dest = @"C:\\Hong\\BiZTalk\\TvavagIntegration\\In\\produkt.txt";

            if (! File.Exists(source))
            {
                MessageBox.Show("Det finns inte filen!");
            }
            else
            {
                File.Copy(source, dest, true);
                MessageBox.Show("copy finish!");
            }            
        }
        string filnamn;
        private void importeraProduktregisterToolStripMenuItem_Click(object sender, EventArgs e)
        {          
            OpenFileDialog of = new OpenFileDialog();
            of.InitialDirectory = "C:\\Hong\\BiZTalk\\TvavagIntegration\\Ut";
            of.Filter = "XML files (.xml)|*.xml";
            of.FilterIndex = 1;

            if (of.ShowDialog() == DialogResult.OK)
            {
                filnamn = of.FileName; // ta filename            
                XMLLoad();               
            }
            else
                ;
        }   
        private void XMLLoad()
        {
            List<Produkt> prolst = new List<Produkt>();
            XmlDocument doc = new XmlDocument();            
            doc.Load(filnamn);
            foreach (XmlNode node in doc.DocumentElement)
            {
                int id = int.Parse(node["ProductID"].InnerText);
                string name = node["Name"].InnerText;
                //int age = int.Parse(node["Age"].Value);// value error
                decimal pris = decimal.Parse(node["Price"].InnerText);
                int antal = int.Parse(node["Count"].InnerText);
                
                prolst.Add(new Produkt(id, name, pris, antal));
            }

            // spara innehållet till textfilen
            sw = new StreamWriter(@"C:\\Hong\\BiZTalk\\TvavagIntegration\\SystemAffar_V2\\SystemAffar\\bin\\Debug\\produkt.txt");
            foreach (Produkt p in prolst)
            {            
                sw.WriteLine(p);            
            }
            sw.Close();
            MessageBox.Show("Importera produktregister färdigt!");
        }

    }
}
