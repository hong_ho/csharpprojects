﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SystemAffar
{
    public partial class KlassDiagram : Form
    {
        public KlassDiagram()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 k = new Form1();
            k.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Lagerarbete k = new Lagerarbete();
            k.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Kassabruk k = new Kassabruk();
            k.Show();
            this.Hide();
        }
    }
}
