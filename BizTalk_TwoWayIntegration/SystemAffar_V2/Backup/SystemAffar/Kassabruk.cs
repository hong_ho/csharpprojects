﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace SystemAffar
{
    public partial class Kassabruk : Form
    {
        StreamReader sr;
        StreamWriter sw;
        List<Produkt> listpro = new List<Produkt>();
        public Kassabruk()
        {            
            InitializeComponent();
            laddaUppFil();
        }
        public void laddaUppFil()
        {
            sr = new StreamReader("produkt.csv");
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string[] linePro = line.Split(';');
                Produkt pr = new Produkt();
                bool isID, isPris, isAntal;
                int k, a;
                decimal pris;

                isID = int.TryParse(linePro[0], out k);
                isPris = decimal.TryParse(linePro[2], out pris);
                isAntal = int.TryParse(linePro[3], out a);
                if (isID == true && isPris == true)
                {
                    pr.VaruID = k;
                    pr.Pris = pris;
                    pr.Antal = a;
                }

                pr.Name = linePro[1];
                listBox1.Items.Add(pr.VaruID+";"+pr.Name + ";" + pr.Pris + "; " + pr.Antal);
              
                listpro.Add(pr);
            }
            sr.Close();
         /*   var pros = from p in listpro
                      
                        select new{ p.VaruId,p.Name,p.Pris,p.Antal};
            
            bindingSource1.DataSource = pros;
            listBox1.DataSource = bindingSource1;*/
        }
        private void btnBuy_Click(object sender, EventArgs e)
        {          
            for (int k = 0; k < listBox1.Items.Count; k++)
            {
                string rad = listBox1.Items[k].ToString();
                string[] ord = rad.Split(';');
              
                for (int j = 0; j < listBox2.Items.Count; j++)
                {
                    string nameLst2=listBox2.Items[j].ToString();
                    string[] n = nameLst2.Split(';');                 

                    if (n[0].Equals(ord[1]))
                    {                       
                        int updateantal = Int32.Parse(ord[2]);
                        int newAntal = updateantal - 1;
                       
                        listBox1.Items.RemoveAt(k);
                        ord[3] = newAntal.ToString();
                        listBox1.Items.Add(ord[0] + ";" + ord[1] + ";" + ord[2] + ";" + ord[3]);
                    }

                }
            }

            sparaListProdukter();

          /*  int sum = 0;
            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                string rad = listBox2.Items[i].ToString();
                string[] ord = rad.Split(';');
                int pris = Int32.Parse(ord[1]);
                sum = sum + pris;               
            }

           StreamWriter read = new StreamWriter("sum.txt",true);
            read.WriteLine(DateTime.Now + "\t" + sum);
            read.Close();
            */

        }
        private void sparaListProdukter()
        {
            sw = new StreamWriter("produkt.txt");
            foreach (string i in listBox1.Items)
                sw.WriteLine(i);
            sw.Close();
        }
        private void btnMove_Click(object sender, EventArgs e)
        {
           
            string rad = listBox1.SelectedItem.ToString();
            string[] ord = rad.Split(';');          
            listBox2.Items.Add(ord[1]+";"+ord[2]);
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            listBox2.Items.Remove(listBox2.SelectedItem);
        }

       
        private void btnHem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void btnLagerarbete_Click(object sender, EventArgs e)
        {
            Lagerarbete l = new Lagerarbete();
            l.Show();
            this.Hide();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString("MediaAffär", new Font("Arial", 24, FontStyle.Regular), Brushes.Black, new Point(25, 100));
            e.Graphics.DrawString("Date: " + DateTime.Now, new Font("Arial", 12), Brushes.Black, new Point(25, 200));
            e.Graphics.DrawString("Address: ", new Font("Arial", 12), Brushes.Black, new Point(25, 220));

            e.Graphics.DrawString(lbldash.Text, new Font("Arial", 12), Brushes.Black, new Point(25, 250));
            e.Graphics.DrawString("Varor", new Font("Arial", 12), Brushes.Black, new Point(25, 280));
            e.Graphics.DrawString("Pris", new Font("Arial", 12), Brushes.Black, new Point(300, 280));
            e.Graphics.DrawString(lbldash.Text, new Font("Arial", 12), Brushes.Black, new Point(25, 300));
            int sum = 0;
            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                string rad = listBox2.Items[i].ToString();
                string[] ord = rad.Split(';');
                int pris = Int32.Parse(ord[1]);              
                sum= sum + pris;                
                e.Graphics.DrawString(ord[0] + "", new Font("Arial", 12), Brushes.Black, new Point(25, i * 20 + 320));
                e.Graphics.DrawString(pris + "", new Font("Arial", 12), Brushes.Black, new Point(300, i * 20 + 320));
            }
            e.Graphics.DrawString(lbldash.Text, new Font("Arial", 12), Brushes.Black, new Point(25, 500));
            e.Graphics.DrawString("Total", new Font("Arial", 12), Brushes.Black, new Point(25, 550));
            e.Graphics.DrawString(sum.ToString(), new Font("Arial", 12), Brushes.Black, new Point(300, 550));
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.Document = printDocument1;
            DialogResult r=printPreviewDialog1.ShowDialog();
            if (r == DialogResult.OK)
                printDocument1.Print();


        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            string s = txtFind.Text;
            IEnumerable<Produkt> produkts = from p in listpro
                                            where p.Name.Contains(s)
                                            select p;
            BindingSource src = new BindingSource(produkts, null);
            listBox3.DataSource = src;
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            int newAntal=0;
               int antal;
             string s = txtFind.Text;
             for (int k = 0; k < listBox1.Items.Count; k++)
             {
                 string rad = listBox1.Items[k].ToString();
                 string[] ord = rad.Split(';');

                 string name = ord[1].ToString();
                 antal = Int32.Parse(ord[3]);// MessageBox.Show("ud" + updateantal);
                 if (name.Equals(txtFind.Text))
                 {                     
                     newAntal = antal + 1; //MessageBox.Show("new" + newAntal);                     
                    
                     listBox1.Items.Add(ord[0] + ";" + ord[1] + ";" + ord[2] + ";" + newAntal);
                     listBox1.Items.RemoveAt(k);
                 }
                 
             }
             sparaListProdukter();
        }

        private void btnTopPro_Click(object sender, EventArgs e)
        {
            IEnumerable<Produkt> produkts = (from p in listpro
                                             orderby p.Name ascending

                                             select p).Take(10);
            BindingSource src = new BindingSource(produkts, null);
            listBox3.DataSource = src;         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            KlassDiagram l = new KlassDiagram();
            l.Show();
            this.Hide();
        }
    }
}
