﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemAffar
{
    class Produkt
    {
        public int VaruID { get; set; }
        public string Name { get; set; }
        public decimal Pris { get; set; }
        public int Antal { get; set; }

        public Produkt()
        {
            this.VaruID = 0;
            this.Name = "";
            this.Pris = 0;
            this.Antal = 0;
        }
        public Produkt(int id, string n, decimal p, int a)
        {
            this.VaruID = id;
            this.Name = n;
            this.Pris = p;
            this.Antal = a;
        }
        public override string ToString()
        {
            //return base.ToString();
            return Name + ";" + Pris + ";" + Antal;
        }

    }
}
