namespace TvavagIntegration {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"TvavagIntegration.File_Simpleaffär", typeof(global::TvavagIntegration.File_Simpleaffär))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"TvavagIntegration.ProduktFlatFileSchema", typeof(global::TvavagIntegration.ProduktFlatFileSchema))]
    public sealed class XMLToTextMap : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var"" version=""1.0"" xmlns:ns0=""http://TvavagIntegration.ProduktFlatFileSchema"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/Inventory"" />
  </xsl:template>
  <xsl:template match=""/Inventory"">
    <ns0:Produkter>
      <xsl:for-each select=""Item"">
        <Produkt>
          <ID>
            <xsl:value-of select=""ProductID/text()"" />
          </ID>
          <Namn>
            <xsl:value-of select=""Name/text()"" />
          </Namn>
          <Pris>
            <xsl:value-of select=""Price/text()"" />
          </Pris>
          <Antal>
            <xsl:value-of select=""Count/text()"" />
          </Antal>
        </Produkt>
      </xsl:for-each>
    </ns0:Produkter>
  </xsl:template>
</xsl:stylesheet>";
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"TvavagIntegration.File_Simpleaffär";
        
        private const string _strTrgSchemasList0 = @"TvavagIntegration.ProduktFlatFileSchema";
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"TvavagIntegration.File_Simpleaffär";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"TvavagIntegration.ProduktFlatFileSchema";
                return _TrgSchemas;
            }
        }
    }
}
