namespace TvavagIntegration {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"TvavagIntegration.ProduktFlatFileSchema", typeof(global::TvavagIntegration.ProduktFlatFileSchema))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"TvavagIntegration.File_Simpleaffär", typeof(global::TvavagIntegration.File_Simpleaffär))]
    public sealed class TextToXmlMap : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:s0=""http://TvavagIntegration.ProduktFlatFileSchema"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Produkter"" />
  </xsl:template>
  <xsl:template match=""/s0:Produkter"">
    <Inventory>
      <xsl:for-each select=""Produkt"">
        <Item>
          <Name>
            <xsl:value-of select=""Namn/text()"" />
          </Name>
          <Count>
            <xsl:value-of select=""Antal/text()"" />
          </Count>
          <Price>
            <xsl:value-of select=""Pris/text()"" />
          </Price>
          <Comment>
            <xsl:text>N/A</xsl:text>
          </Comment>
          <Artist>
            <xsl:text>N/A</xsl:text>
          </Artist>
          <Publisher>
            <xsl:text>N/A</xsl:text>
          </Publisher>
          <Genre>
            <xsl:text>N/A</xsl:text>
          </Genre>
          <Year>
            <xsl:text>0</xsl:text>
          </Year>
          <ProductID>
            <xsl:value-of select=""ID/text()"" />
          </ProductID>
        </Item>
      </xsl:for-each>
    </Inventory>
  </xsl:template>
</xsl:stylesheet>";
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"TvavagIntegration.ProduktFlatFileSchema";
        
        private const string _strTrgSchemasList0 = @"TvavagIntegration.File_Simpleaffär";
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"TvavagIntegration.ProduktFlatFileSchema";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"TvavagIntegration.File_Simpleaffär";
                return _TrgSchemas;
            }
        }
    }
}
