﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lotto
{
    public partial class Form1 : Form
    {
        int[] txtRad = new int[7];
        int[] lottoRad = new int[7];
        Random r = new Random();
        int antalDrag;
        int femRatt=0, sexRatt=0, sjuRatt=0;
       
        public Form1()
        {
            InitializeComponent();            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            checkNr();            
            checkUnik();         
            skrivUtRatt(antalDrag);          
        }
       void checkNr()
        {
            try {
                txtRad[0] = Int32.Parse(txt1.Text);
                txtRad[1] = Int32.Parse(txt2.Text);
                txtRad[2] = Int32.Parse(txt3.Text);
                txtRad[3] = Int32.Parse(txt4.Text);
                txtRad[4] = Int32.Parse(txt5.Text);
                txtRad[5] = Int32.Parse(txt6.Text);
                txtRad[6] = Int32.Parse(txt7.Text);
                for (int i = 0; i < 7; i++)
                {
                    if (txtRad[i] > 35 || txtRad[i] <= 0)
                    {                       
                        throw new Exception();
                    }
                }
            }
            catch (Exception)
            { MessageBox.Show("Måste mata in lottonummer i integer, inom 1-35!"); }   

        }      
       void checkUnik()
       {          
           for (int i = 0; i < 7; i++)
           {
               int tal = txtRad[i];
               for (int j = 0; j < i; j++)
                   if (txtRad[i] == txtRad[j])
                   {
                       MessageBox.Show("Lottonummer får inte vara dubbelt!");
                   }                   
           }
       }     
       private void txt8_Validating(object sender, CancelEventArgs e)
        {
              try
             {
                 antalDrag = Int32.Parse(txt8.Text);
                 if (antalDrag <= 0)
                     MessageBox.Show("antalDrag får inte vara negativ!");
             }
             catch (Exception)
             {
                 MessageBox.Show("Måste mata in antalDrag i interger!");
             }
           /* int val = 0;
               bool res = Int32.TryParse(txt8.Text, out val);
               if (res == true)
                   ;
               else
               {
                   MessageBox.Show("Mata in antalDrag i integer!");
                  // return;
               }*/
        }
       void clear()
        {
            txt1.Text = "";
            txt2.Text = "";
            txt3.Text = "";
            txt4.Text = "";
            txt5.Text = "";
            txt6.Text = "";
            txt7.Text = "";
            txt8.Text = "";
        }
       void slumpLottorad()
        {
          int ejUnik,i,j;
	      for(i=0; i<7; i++)
		  {
		    do
		    {
		      ejUnik=0;		
		      lottoRad[i] = r.Next(1,35);// ta första random till i=0 först
		      for(j=0; j<i; j++)// ktra
		      {
		        if(lottoRad[i]==lottoRad[j])
		        {
		          ejUnik=1;		   // neu gap trung so       
		        }
		      }
		    }while(ejUnik==1);     // lam khi ko co u                  
		  }        
        }
       void checkRight()
        {
            int lottoNr = 0;
            int ratt = 0;
                for (int i = 0; i < 7; i++)
                {
                    lottoNr = txtRad[i];
                    if (lottoRad.Contains(lottoNr))
                        ratt++;                   
                }
               if (ratt == 5) { femRatt++; }
               if (ratt == 6) { sexRatt++; }
               if (ratt == 7) { sjuRatt++; }   
            txt9.Text = femRatt.ToString();
            txt10.Text = sexRatt.ToString();
            txt11.Text = sjuRatt.ToString();           
        }
       void skrivUtRatt(int antalDraw)
        {            
            for (int i = 0; i < antalDraw; i++)
            {
                slumpLottorad();
                checkRight();                
            }            
        }     
    }
}
