﻿namespace Lotto
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt8 = new System.Windows.Forms.TextBox();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.txt4 = new System.Windows.Forms.TextBox();
            this.txt5 = new System.Windows.Forms.TextBox();
            this.txt6 = new System.Windows.Forms.TextBox();
            this.txt7 = new System.Windows.Forms.TextBox();
            this.txt9 = new System.Windows.Forms.TextBox();
            this.txt10 = new System.Windows.Forms.TextBox();
            this.txt11 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(359, 79);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 31);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start Lotto";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Din Lottorad:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Antal dragningar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "5 rätt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(241, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "6 rätt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(358, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "7 rätt";
            // 
            // txt8
            // 
            this.txt8.Location = new System.Drawing.Point(186, 85);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(142, 20);
            this.txt8.TabIndex = 6;
            this.txt8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt8.Validating += new System.ComponentModel.CancelEventHandler(this.txt8_Validating);
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(87, 23);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(53, 20);
            this.txt1.TabIndex = 7;
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(158, 23);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(53, 20);
            this.txt2.TabIndex = 8;
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(229, 23);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(53, 20);
            this.txt3.TabIndex = 9;
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(298, 23);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(53, 20);
            this.txt4.TabIndex = 10;
            // 
            // txt5
            // 
            this.txt5.Location = new System.Drawing.Point(371, 23);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(53, 20);
            this.txt5.TabIndex = 11;
            // 
            // txt6
            // 
            this.txt6.Location = new System.Drawing.Point(442, 23);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(53, 20);
            this.txt6.TabIndex = 12;
            // 
            // txt7
            // 
            this.txt7.Location = new System.Drawing.Point(513, 23);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(53, 20);
            this.txt7.TabIndex = 13;
            // 
            // txt9
            // 
            this.txt9.Location = new System.Drawing.Point(164, 165);
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(53, 20);
            this.txt9.TabIndex = 14;
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(278, 165);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(53, 20);
            this.txt10.TabIndex = 15;
            // 
            // txt11
            // 
            this.txt11.Location = new System.Drawing.Point(395, 165);
            this.txt11.Name = "txt11";
            this.txt11.Size = new System.Drawing.Size(53, 20);
            this.txt11.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(581, 211);
            this.Controls.Add(this.txt11);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt9);
            this.Controls.Add(this.txt7);
            this.Controls.Add(this.txt6);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.txt8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt8;
        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt3;
        private System.Windows.Forms.TextBox txt4;
        private System.Windows.Forms.TextBox txt5;
        private System.Windows.Forms.TextBox txt6;
        private System.Windows.Forms.TextBox txt7;
        private System.Windows.Forms.TextBox txt9;
        private System.Windows.Forms.TextBox txt10;
        private System.Windows.Forms.TextBox txt11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
    }
}

