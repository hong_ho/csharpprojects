﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Fil_hantering
{
    public partial class Form1 : Form
    {
        string filename = "dok.1";
        OpenFileDialog op;
        StreamWriter sw;
        DialogResult result;
        
        public Form1()
        {           
            InitializeComponent();
            this.Text = filename;
            label1.Text = "";
            richTextBox1.DragDrop += new DragEventHandler(richTextBox1_DragDrop);
            richTextBox1.AllowDrop = true;
        }
        /*
         * Version 1: gör alla items i menyn
         * New - Open - Save - Save As -Close
         * */
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            int cs, c, w, l; 
            this.Text = "*" + filename;
           
            c=characterWithoutSpace();
            cs=characterWithSpace();
            l= lineCount();
            w=wordCount();
            label1.Text = "Antal bokstäver med mellanslag: " + cs + "\n" +
                          "Antal bokstäver utan mellanslag: " + c + "\n" +
                          "Antal ord: " + w + "\n" +
                          "Antal rader: " + l;
        }        
        public void open()
        {            
            op = new OpenFileDialog();
            if (op.ShowDialog() == DialogResult.OK)
            {
                StreamReader read = new StreamReader(op.FileName);
                richTextBox1.Text = read.ReadToEnd();
                filename = op.FileName;
                this.Text = filename;
                read.Close();
            }
        }
        public void saveAs()
        {
            SaveFileDialog sf=new SaveFileDialog();
            sf.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            sf.CheckFileExists = false;        

            if (sf.ShowDialog() == DialogResult.OK)
            {
                filename = sf.FileName;
                if (filename == String.Empty)
                {
                    MessageBox.Show("Invalid fileName", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
					
                        FileStream outfile = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);
                        sw = new StreamWriter(outfile);
                        sw.WriteLine(richTextBox1.Text);
                        sw.Close();
                        this.Text = filename;                       
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("Error opening file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }                 
        }
        public void save()
        {
            sw = new StreamWriter(filename);
            sw.Write(richTextBox1.Text);
            this.Text = filename;
            sw.Close();                  
        } 
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filename = "dok.1";
            if(this.Text=="*dok.1")
            {
                if (richTextBox1.Text == "")
                    this.Text = "dok.1";
                else
                {                    
                     result= MessageBox.Show("Do you want to save?", "Confirmation", MessageBoxButtons.YesNoCancel);
                    if(result == DialogResult.Yes)
                    {    
                        saveAs();
                        richTextBox1.Text = "";
                        this.Text = filename;
                    }
                    else if (result == DialogResult.No)
                    {
                        richTextBox1.Text = "";
                        this.Text = filename;
                    }
                    else
                        ;
                }
            }
           else if (this.Text == "*" + filename)
           {
                 result = MessageBox.Show("Do you want to save changes?", "Confirmation", MessageBoxButtons.YesNoCancel);
                 if (result == DialogResult.Yes)
                  {
                      save();
                      richTextBox1.Text = "";
                      this.Text = filename;
                  }
                  else if (result == DialogResult.No)
                  {
                     richTextBox1.Text = "";
                     this.Text = filename;                    
                  }
                  else
                        ;
             }
            else
            {
                richTextBox1.Text = "";               
                this.Text = filename;
            }            
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
           if(this.Text=="*dok.1")
            {
                if (richTextBox1.Text == "")
                    open();
                else
                {                    
                     result= MessageBox.Show("Do you want to save?", "Confirmation", MessageBoxButtons.YesNoCancel);
                    if(result == DialogResult.Yes)
                    {    
                        saveAs();
                        open();
                    }
                    else if (result == DialogResult.No)
                    {
                        open();
                    }
                    else
                        ;
                }
            }
           else if (this.Text == "*" + filename)
           {                  
                 result = MessageBox.Show("Do you want to save changes?", "Confirmation", MessageBoxButtons.YesNoCancel);
                 if (result == DialogResult.Yes)
                  {
                      save();
                      open();
                  }
                  else if (result == DialogResult.No)
                  {
                      open();
                  }
                  else
                        ;
             }
            else
            {
                open();
            }            
        }     
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Text == "*dok.1")
            {
                saveAs();
            }
            else
                save();
        }
        private void saveASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveAs();
        }
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {           
            if (this.Text == "*dok.1")
            {
                if (richTextBox1.Text != "")
                {
                     result= MessageBox.Show("Do you want to save changes dok?", "Confirmation", MessageBoxButtons.YesNoCancel);
                    if(result == DialogResult.Yes)
                    {    
                        saveAs();                      
                        Application.Exit();
                    }
                    else if(result == DialogResult.No)
                        Application.Exit();
                    else
                        ;
                }
                else
                {
                    Application.Exit();
                }
            }
            else if (this.Text == "*" + filename)
                {                   
                    result = MessageBox.Show("Do you want to save changes?", "Confirmation", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                    {
                        save();
                    }
                    else if (result == DialogResult.No)
                        Application.Exit();
                    else
                        ;
                }
            else
                Application.Exit();            
        }
       
        /*
        * VERSION 2 : returnerade funktionerna som ger värderna till text_changed funktionen i version o
         * update värderna när richTextBox1 ändrar texter
        * */
        public int characterWithoutSpace()
        {
            string st = richTextBox1.Text;
            st = st.Trim();
            string[] wordCount = st.Split(null);
            int charCount = 0;

            foreach (var word in wordCount)
                charCount += word.Length;
            return charCount;
        }
        public int wordCount()
        {
            string userInput = richTextBox1.Text;
            userInput = userInput.Trim();
            string[] wordCount = userInput.Split(null);
            return wordCount.Length;
        }
        public int characterWithSpace()
        {
            string tb = richTextBox1.Text;
            int ch = 0;
            for (int c = 0; c < tb.Length; c++)
            {
                ch++;
            }
            return ch;
        }
        public int lineCount()
        {
            int lineCount = richTextBox1.Lines.Count();
            return lineCount;
        }
       
        
        /*
        * VERSION 3: 
        * Stäng systemet med krysset "X"
        * Dra en textfil från mapperna eller skrivbordet i datorn - använd Ctr och Shift
        * */
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Text == "*dok.1")
            {
                if (richTextBox1.Text != "")
                {
                    result = MessageBox.Show("Do you want to save?", "Confirmation", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                    {
                        saveAs();
                        Application.Exit();
                    }
                    else if (result == DialogResult.No)
                        Application.Exit();
                    else
                        ;
                }
                else
                {
                    Application.Exit();
                }
            }
            else if (this.Text == "*" + filename)
            {
                result = MessageBox.Show("Do you want to save changes?", "Confirmation", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    save();
                }
                else if (result == DialogResult.No)
                    Application.Exit();
                else
                    ;
            }
            else
                Application.Exit();
        }

        bool shiftFlag = false;
        bool ctrFlag = false;
        void richTextBox1_DragDrop(object sender, DragEventArgs e)
        {
         
            if (ctrFlag == true)
            {
                // MessageBox.Show("ctrol");
                string[] fileName = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                foreach (string file in fileName)
                {
                    StreamReader sr = new StreamReader(file);
                    richTextBox1.Text += sr.ReadToEnd();
                }               
            }
            else if (shiftFlag == true)
            {
                string[] fileName = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                foreach (string file in fileName)
                {
                    StreamReader sr = new StreamReader(file);
                    richTextBox1.Text += sr.ReadToEnd();
                }
            }
            else
            {
                  if (this.Text == "*dok.1")
                     {
                         result = MessageBox.Show("Do you want to save changes?", "Confirmation", MessageBoxButtons.YesNo);
                         if (result == DialogResult.Yes)
                         {                  
                             saveAs();
                             Application.Exit();
                         }
                         else
                             Application.Exit();
                     }
                     else if (this.Text == "*" + filename)
                     {
                         result = MessageBox.Show("Do you want to save changes?", "Confirmation", MessageBoxButtons.YesNo);
                         if (result == DialogResult.Yes)
                         {                  
                             save();
                             Application.Exit();
                         }
                         else
                             Application.Exit();
                     }               
                    else
                            Application.Exit();
            }
        }
       
        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Shift)
            {
                // MessageBox.Show("shif hej");
                shiftFlag = true;
                richTextBox1.SelectedText = "";
                save();
            }
            else if (e.Control)
            {
                ctrFlag = true;
                //  MessageBox.Show("ctr hej");
            }           
        }
    }
}
