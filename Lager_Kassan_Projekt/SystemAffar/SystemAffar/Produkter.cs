﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemAffar
{
    class Produkter
    {
        private Produkt produkt;
        public List<Produkt> produkter { get; set; }
        public int Antal { get; set; }

        Produkter()
        {
            produkt = new Produkt();
            produkter = new List<Produkt>();
        }
        public void addprodukt(Produkt p)
        {
            produkter.Add(p);
        }
        public void removeProdukt(int pos)
        {
            produkter.RemoveAt(pos);
        }
       
    }
}
