﻿namespace SystemAffar
{
    partial class KlassDiagram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnHem = new System.Windows.Forms.Button();
            this.btnLager = new System.Windows.Forms.Button();
            this.btnKassa = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHem
            // 
            this.btnHem.Location = new System.Drawing.Point(651, 28);
            this.btnHem.Name = "btnHem";
            this.btnHem.Size = new System.Drawing.Size(91, 44);
            this.btnHem.TabIndex = 1;
            this.btnHem.Text = "Hem";
            this.btnHem.UseVisualStyleBackColor = true;
            this.btnHem.Click += new System.EventHandler(this.btnHem_Click);
            // 
            // btnLager
            // 
            this.btnLager.Location = new System.Drawing.Point(651, 78);
            this.btnLager.Name = "btnLager";
            this.btnLager.Size = new System.Drawing.Size(91, 44);
            this.btnLager.TabIndex = 2;
            this.btnLager.Text = "LagerArbete";
            this.btnLager.UseVisualStyleBackColor = true;
            this.btnLager.Click += new System.EventHandler(this.btnLager_Click);
            // 
            // btnKassa
            // 
            this.btnKassa.Location = new System.Drawing.Point(651, 128);
            this.btnKassa.Name = "btnKassa";
            this.btnKassa.Size = new System.Drawing.Size(91, 44);
            this.btnKassa.TabIndex = 3;
            this.btnKassa.Text = "KassaBruk";
            this.btnKassa.UseVisualStyleBackColor = true;
            this.btnKassa.Click += new System.EventHandler(this.btnKassa_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SystemAffar.Properties.Resources.klassDiagram;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(642, 467);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // KlassDiagram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 482);
            this.Controls.Add(this.btnKassa);
            this.Controls.Add(this.btnLager);
            this.Controls.Add(this.btnHem);
            this.Controls.Add(this.pictureBox1);
            this.Name = "KlassDiagram";
            this.Text = "KlassDiagram";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnHem;
        private System.Windows.Forms.Button btnLager;
        private System.Windows.Forms.Button btnKassa;
    }
}