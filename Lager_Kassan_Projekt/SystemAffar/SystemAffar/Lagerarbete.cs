﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic;
namespace SystemAffar
{
    public partial class Lagerarbete : Form
    {
        
        List<Produkt> listpro = new List<Produkt>();
        private StreamReader sr;
        private StreamWriter sw;
        
        public Lagerarbete()
        {
            InitializeComponent();
           
        }
        /*
         * Lägga till en produkt 
         * */
        private void btnSave_Click(object sender, EventArgs e)
        {
            checkInMata();
        }
        private void checkInMata()
        {
             bool test = false;
            //kunna mata in om filen är tom i början
            FileInfo info = new FileInfo("produkt.txt");
            if (info.Length == 0)
            {
                skrivIn();
                test = true;
            }
            else
            {
                string[] linePro;
                bool flag = false;
                sr = new StreamReader("produkt.txt");
                while (!sr.EndOfStream && test == false)
                {
                    string line = sr.ReadLine();
                    linePro = line.Split(';');

                    string ids = linePro[0];
                    string name = linePro[1];

                    if (txtID.Text.Equals(ids))
                    {
                        MessageBox.Show("inte unikt ID");
                        flag = false;
                        break;
                    }
                    else if (txtName.Text.Equals(name))
                    {
                        MessageBox.Show("inte unikt namn");
                        flag = false;
                        break;
                    }
                    else
                    {
                        flag = true;
                    }
                }
                sr.Close();
                if (flag == true)
                {
                    skrivIn();
                }
            }
        }
        /*
         * Testa rätt inmatningar av en produkt och skriva varje produkt till filen
         * */
        private void skrivIn()
        {
            Produkt p = new Produkt();
            bool isID, isPris, isAntal;
            int id, antal;
            decimal pris;

            isID = int.TryParse(txtID.Text, out id);
            isPris = decimal.TryParse(txtPris.Text, out pris);
            isAntal = int.TryParse(txtAntal.Text, out antal);
            string name = txtName.Text;

            if (isID == true && isAntal == true)
            {
                p.VaruID = id;
                p.Name = name;
                p.Pris = pris;
                p.Antal = antal;
                listpro.Add(p);               
                skrivProdukt(p.VaruID, p.Name, p.Pris, p.Antal);
                ClearTextboxes();
            }
            else
                MessageBox.Show("ID:int - Pris:Decimai - Antal:int");
        }
        public void skrivProdukt(int id, string name, decimal pris, int antal)
        {
            sw = new StreamWriter("produkt.txt", true);
            sw.WriteLine(id + ";" + name + ";" + pris + ";" + antal);
            sw.Close();
        }
       public void ClearTextboxes()
        {
           foreach(Control tbox in Controls)
           {
               if (tbox is TextBox)
                   ((TextBox)tbox).Clear();
           }
        }       
      
        /*
         * Lista upp alla produkter in i DataGridView
         * */
        private void btnList_Click(object sender, EventArgs e)
        {
            laddaUppFil();
        }
        public void laddaUppFil()
        {
            sr = new StreamReader("produkt.txt");
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string[] linePro = line.Split(';');
                Produkt newProdukt = new Produkt();
                bool isID, isPris, isAntal;
                int k, a;
                decimal pris;

                isID = int.TryParse(linePro[0], out k);
                isPris = decimal.TryParse(linePro[2], out pris);
                isAntal = int.TryParse(linePro[3], out a);
                if (isID == true && isPris == true)
                {
                    newProdukt.VaruID = k;
                    newProdukt.Pris = pris;
                    newProdukt.Antal = a;
                }

                newProdukt.Name = linePro[1];
                listpro.Add(newProdukt);
            }
            sr.Close();

            bindingSource1.DataSource = listpro;
            dataGridView1.DataSource = bindingSource1;
        }
        /*
         * Ta bort en produkt
         * */
        private void btnDelete_Click(object sender, EventArgs e)
        {            
           foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
            {                
                if (oneCell.Selected)
                {
                    int value = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow selectR = dataGridView1.Rows[value];                 
                    int va = (int)selectR.Cells["antal"].Value;
                    if(va>0)
                    {
                        DialogResult r = MessageBox.Show("Vill ta bort?", "Delete", MessageBoxButtons.YesNo);
                        if(r==DialogResult.Yes)
                        {                            
                            dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
                        }
                        else
                        {
                            ;
                        }
                    }
                    else
                    {
                        dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
                    }
                }                   
            }
           SaveDatagridView();
        }
        public void SaveDatagridView()
        {
            sw = new StreamWriter("produkt.txt");
            for (int x = 0; x < dataGridView1.Rows.Count - 1; x++)
            {
                for (int y = 0; y < dataGridView1.Columns.Count; y++)
                {
                    sw.Write(dataGridView1.Rows[x].Cells[y].Value.ToString());
                    if (y != dataGridView1.Columns.Count - 1)
                    {
                        sw.Write(";");
                    }
                }
                sw.WriteLine();
            }
            sw.Close();       
        }

        /*
         * Ändra Antal av produkterna - select antal från datagridviewcolumn och trycker på knappen Antal
         * */
        private void btnChange_Click(object sender, EventArgs e)
        {
            string antal = Interaction.InputBox("antal:");
            foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
            {

                if (oneCell.Selected)
                {
                  
                    int value = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow selectR = dataGridView1.Rows[value];
                    selectR.Cells["antal"].Value=antal;                    
                    /*int value = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow selectR = dataGridView1.Rows[value];
                    // string va = Convert.ToString(selectR.Cells["antal"].Value);
                    int id = (int)selectR.Cells["varuId"].Value;
                    string name = (string)selectR.Cells["name"].Value;
                    decimal pris = (decimal)selectR.Cells["pris"].Value;
                    int antal = (int)selectR.Cells["antal"].Value;

                    txtID.Text = id.ToString();
                    txtName.Text = name;
                    txtPris.Text = pris.ToString();
                    txtAntal.Text = antal.ToString();*/                   
                }
            }
            SaveDatagridView();
        }

        private void btnHem_Click(object sender, EventArgs e)
        {
            Hem f = new Hem();
            f.Show();
            this.Hide();
        }

        private void btnKassabruk_Click(object sender, EventArgs e)
        {
            Kassabruk k = new Kassabruk();
            k.Show();
            this.Hide();
        }

        private void btnKlassDiagram_Click(object sender, EventArgs e)
        {
            KlassDiagram kd = new KlassDiagram();
            kd.Show();
            this.Hide();
        }
    }
}
