﻿namespace SystemAffar
{
    partial class Kassabruk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Kassabruk));
            this.label1 = new System.Windows.Forms.Label();
            this.affarList = new System.Windows.Forms.ListBox();
            this.korgList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBuy = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnHem = new System.Windows.Forms.Button();
            this.btnLagerarbete = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.btnTop = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.lblDash = new System.Windows.Forms.Label();
            this.txtAntal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnKlassDiagram = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(393, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Välkommen Till Kassabruk";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // affarList
            // 
            this.affarList.FormattingEnabled = true;
            this.affarList.Location = new System.Drawing.Point(6, 76);
            this.affarList.Name = "affarList";
            this.affarList.Size = new System.Drawing.Size(112, 173);
            this.affarList.TabIndex = 1;
            // 
            // korgList
            // 
            this.korgList.FormattingEnabled = true;
            this.korgList.Location = new System.Drawing.Point(286, 76);
            this.korgList.Name = "korgList";
            this.korgList.Size = new System.Drawing.Size(112, 173);
            this.korgList.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.label2.Location = new System.Drawing.Point(17, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "List Varor";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.label3.Location = new System.Drawing.Point(298, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Korg";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBuy
            // 
            this.btnBuy.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuy.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnBuy.Location = new System.Drawing.Point(124, 222);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(153, 27);
            this.btnBuy.TabIndex = 5;
            this.btnBuy.Text = "Köp Varor";
            this.btnBuy.UseVisualStyleBackColor = false;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(226, 106);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(31, 28);
            this.button2.TabIndex = 6;
            this.button2.Text = "->";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.move_click);
            // 
            // btnHem
            // 
            this.btnHem.Location = new System.Drawing.Point(406, 6);
            this.btnHem.Name = "btnHem";
            this.btnHem.Size = new System.Drawing.Size(68, 24);
            this.btnHem.TabIndex = 9;
            this.btnHem.Text = "Hem";
            this.btnHem.UseVisualStyleBackColor = true;
            this.btnHem.Click += new System.EventHandler(this.btnHem_Click);
            // 
            // btnLagerarbete
            // 
            this.btnLagerarbete.Location = new System.Drawing.Point(480, 6);
            this.btnLagerarbete.Name = "btnLagerarbete";
            this.btnLagerarbete.Size = new System.Drawing.Size(76, 24);
            this.btnLagerarbete.TabIndex = 10;
            this.btnLagerarbete.Text = "Lagerarbete";
            this.btnLagerarbete.UseVisualStyleBackColor = true;
            this.btnLagerarbete.Click += new System.EventHandler(this.btnLagerarbete_Click);
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(488, 120);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(153, 134);
            this.listBox3.TabIndex = 11;
            // 
            // txtFind
            // 
            this.txtFind.Location = new System.Drawing.Point(488, 60);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(153, 20);
            this.txtFind.TabIndex = 12;
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(582, 87);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(59, 27);
            this.btnFind.TabIndex = 13;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(488, 86);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(74, 28);
            this.btnReturn.TabIndex = 14;
            this.btnReturn.Text = "ÅterKöp";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // btnTop
            // 
            this.btnTop.Location = new System.Drawing.Point(487, 264);
            this.btnTop.Name = "btnTop";
            this.btnTop.Size = new System.Drawing.Size(154, 30);
            this.btnTop.TabIndex = 15;
            this.btnTop.Text = "Top 10 Produkter";
            this.btnTop.UseVisualStyleBackColor = true;
            this.btnTop.Click += new System.EventHandler(this.btnTop_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(124, 178);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(153, 27);
            this.btnPrint.TabIndex = 16;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // lblDash
            // 
            this.lblDash.AutoSize = true;
            this.lblDash.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDash.Location = new System.Drawing.Point(3, 328);
            this.lblDash.Name = "lblDash";
            this.lblDash.Size = new System.Drawing.Size(529, 13);
            this.lblDash.TabIndex = 17;
            this.lblDash.Text = "---------------------------------------------------------------------------------" +
    "--------------------------------------------------------------------------------" +
    "-------------";
            // 
            // txtAntal
            // 
            this.txtAntal.Location = new System.Drawing.Point(144, 126);
            this.txtAntal.Name = "txtAntal";
            this.txtAntal.Size = new System.Drawing.Size(59, 20);
            this.txtAntal.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(144, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 26);
            this.label4.TabIndex = 19;
            this.label4.Text = "Antal";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnKlassDiagram
            // 
            this.btnKlassDiagram.Location = new System.Drawing.Point(562, 6);
            this.btnKlassDiagram.Name = "btnKlassDiagram";
            this.btnKlassDiagram.Size = new System.Drawing.Size(79, 24);
            this.btnKlassDiagram.TabIndex = 20;
            this.btnKlassDiagram.Text = "KlassDiagram";
            this.btnKlassDiagram.UseVisualStyleBackColor = true;
            this.btnKlassDiagram.Click += new System.EventHandler(this.btnKlassDiagram_Click);
            // 
            // Kassabruk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(670, 300);
            this.Controls.Add(this.btnKlassDiagram);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAntal);
            this.Controls.Add(this.lblDash);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnTop);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtFind);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.btnLagerarbete);
            this.Controls.Add(this.btnHem);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnBuy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.korgList);
            this.Controls.Add(this.affarList);
            this.Controls.Add(this.label1);
            this.Name = "Kassabruk";
            this.Text = "Kassabruk";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox affarList;
        private System.Windows.Forms.ListBox korgList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnHem;
        private System.Windows.Forms.Button btnLagerarbete;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Button btnTop;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.Label lblDash;
        private System.Windows.Forms.TextBox txtAntal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnKlassDiagram;
    }
}