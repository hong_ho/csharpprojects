﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace SystemAffar
{
    public partial class Kassabruk : Form
    {
       private StreamReader sr;
       private StreamWriter sw;
       private List<Produkt> listpro = new List<Produkt>();
      // Produkt nyP = new Produkt();
       List<Produkt> listKorg = new List<Produkt>();

       public Kassabruk()
        {            
            InitializeComponent();
            laddaUppFil();
        }
        public void laddaUppFil()
        {
            sr = new StreamReader("produkt.txt");
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string[] linePro = line.Split(';');                
                bool isID, isPris, isAntal;
                int k, a;
                decimal pris;
                Produkt newProdukt = new Produkt();
                isID = int.TryParse(linePro[0], out k);
                isPris = decimal.TryParse(linePro[2], out pris);
                isAntal = int.TryParse(linePro[3], out a);
                if (isID == true && isPris == true)
                {
                    newProdukt.VaruID = k;
                    newProdukt.Pris = pris;
                    newProdukt.Antal = a;
                }
                newProdukt.Name = linePro[1];
                listpro.Add(newProdukt);
            }
            sr.Close();
            bindingSource1.DataSource = listpro;
            affarList.DataSource = bindingSource1;           
        }
        int antalKassa = 1;
       private void move_click(object sender, EventArgs e)        
       {  /* 
           string rad = affarList.SelectedItem.ToString();
           string[] ord = rad.Split(';');
           int antalLager = Int32.Parse(ord[3]);
           string name = ord[1];
           int id = Int32.Parse(ord[0]);
           decimal pris = Decimal.Parse(ord[2]);
          int antalKassa=0;
           
            if(txtAntal.Text=="")
            { MessageBox.Show("Hur många produkter vill du köpa?");  }   
            else
            {
                antalKassa = Int32.Parse(txtAntal.Text);
                if (antalKassa > antalLager)
                {
                    MessageBox.Show("Finns inte så många produkter i lagret :-("); return;
                }
                else
                {
                    listKorg.Add(new Produkt(id, name, pris, antalKassa));                 
                 }        
            }          
            BindingSource bs = new BindingSource();            
            bs.DataSource = listKorg;
            korgList.DataSource = bs;   */
           Produkt prod = new Produkt();
           string rad = affarList.SelectedItem.ToString();
           string[] ord = rad.Split(';');
           int antalLager = Int32.Parse(ord[3]);
           string name = ord[1];
           int id = Int32.Parse(ord[0]);
           decimal pris = Decimal.Parse(ord[2]);
         //  MessageBox.Show(name);
           
               prod.VaruID=id;
               prod.Name=name;
               prod.Pris=pris;
               prod.Antal=antalKassa;
               if (listKorg.Count == 0)
                   listKorg.Add(prod);
               else
               {
                   foreach (Produkt pr in listKorg)
                   {

                       if (pr.Name.Equals(name))
                       {
                         //  pr.VaruID = id;
                         //  pr.Name = name;
                         //  pr.Pris = pris;
                           //  pr.Antal = antalKassa;
                           pr.Antal++; MessageBox.Show("new antal: "+pr.Antal);
                          // listKorg.Add(pr);
                       }
                       else
                       {
                           pr.VaruID = id;
                           pr.Name = name;
                           pr.Pris = pris;
                           pr.Antal = antalKassa; MessageBox.Show("old antal: " + pr.Name+" "+pr.Antal);
                           //listKorg.Add(pr);
                       }
                   }
               }
               BindingSource bs = new BindingSource();
               bs.DataSource = listKorg;
               korgList.DataSource = bs;      
           
        }       
        private void btnBuy_Click(object sender, EventArgs e)
        {
            if(korgList.Items.Count==0)
            {
                MessageBox.Show("Ingen varor i korgen!");
            }
            else
            {
                for (int i = 0; i < korgList.Items.Count; i++)
                {
                    string n = korgList.Items[i].ToString();             
                    string[] ord = n.Split(';');
                    string name = ord[1]; //MessageBox.Show(name);
                    int antal = Int32.Parse(ord[3]);
              
                    List<Produkt> newl = new List<Produkt>();
                    foreach (Produkt item in listpro)//.Where(c => c.Name == name))
                    {
                        if (item.Name.Equals(name))
                        {                        
                            item.Antal = item.Antal - antal;                      
                            newl.Add(item);
                        }
                        else
                        {                      
                            newl.Add(item);
                        }
                    }
                    sw = new StreamWriter("produkt.txt");
                    foreach (Produkt k in newl)
                        sw.WriteLine(k);
                    sw.Close();
            
                   bindingSource1.DataSource = null;                   
                   bindingSource1.DataSource = newl;
                   affarList.DataSource = bindingSource1;                
                }                   
            }

            // tömma varukorgen när kunden har köpt
            txtAntal.Text = "";
            listKorg.Clear();
            BindingSource bs = new BindingSource();
            bs.DataSource = listKorg;
            korgList.DataSource = bs; 
        }
        private void btnHem_Click(object sender, EventArgs e)
        {
            Hem f = new Hem();
            f.Show();
            this.Hide();
        }
        private void btnLagerarbete_Click(object sender, EventArgs e)
        {
            Lagerarbete l = new Lagerarbete();
            l.Show();
            this.Hide();
        }
        private void btnKlassDiagram_Click(object sender, EventArgs e)
        {
            KlassDiagram kd = new KlassDiagram();
            kd.Show();
            this.Hide();
        }
        /*
         * Version 2
         * */
        /*
         * Hittta produkter som matchar namn med textbox
         * */
        private void btnFind_Click(object sender, EventArgs e)
        {
            string s = txtFind.Text;
            IEnumerable<Produkt> produkts = from p in listpro
                                            where p.Name.Contains(s)
                                            select p;
             BindingSource src = new BindingSource(produkts, null);              
             listBox3.DataSource = src;            
        }
        /*
         * Återköp - Mata in produktens namn i textbox och trycker på återköp -> produkten i lagret ökar ett antal.
         * */
        private void btnReturn_Click(object sender, EventArgs e)
        {
            string s = txtFind.Text;           
            List<Produkt> nyl = new List<Produkt>();
            foreach (Produkt item in listpro)
             {
                if (item.Name.Equals(s))
                    {
                        item.Antal = item.Antal + 1;
                        nyl.Add(item);
                    }
                    else
                    {
                        nyl.Add(item);
                    }
                }
                sw = new StreamWriter("produkt.txt");
                foreach (Produkt k in nyl)
                    sw.WriteLine(k);
                sw.Close();
                bindingSource1.DataSource = null;               
                bindingSource1.DataSource = nyl;
                affarList.DataSource = bindingSource1;
                txtFind.Text = "";
        }
              
        /*
        * Kvittofunktionen
        * */
        private void btnPrint_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.Document = printDocument1;
            DialogResult r = printPreviewDialog1.ShowDialog();
            if (r == DialogResult.OK)
                printDocument1.Print();
        }       
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString("MediaAffär", new Font("Arial", 24, FontStyle.Regular), Brushes.Black, new Point(25, 100));
            e.Graphics.DrawString("Date: " + DateTime.Now, new Font("Arial", 12), Brushes.Black, new Point(25, 200));
            e.Graphics.DrawString("Address: Karlstads Univesitet ", new Font("Arial", 12), Brushes.Black, new Point(25, 220));

            e.Graphics.DrawString(lblDash.Text, new Font("Arial", 12), Brushes.Black, new Point(25, 250));
            e.Graphics.DrawString("Varor", new Font("Arial", 12), Brushes.Black, new Point(25, 280));
             e.Graphics.DrawString("Pris", new Font("Arial", 12), Brushes.Black, new Point(300, 280));
            e.Graphics.DrawString(lblDash.Text, new Font("Arial", 12), Brushes.Black, new Point(25, 300));
            decimal sum = 0;
            for (int i = 0; i < korgList.Items.Count; i++)
            {
                string rad = korgList.Items[i].ToString();
                  string[] ord = rad.Split(';');
                 int antal = Int32.Parse(ord[3]);
                 decimal pris = Decimal.Parse(ord[2]);
                 decimal kost = antal * pris;
                 sum = sum + kost;
                
                   e.Graphics.DrawString(ord[1] + "", new Font("Arial", 12), Brushes.Black, new Point(25, i * 20 + 320));
                   e.Graphics.DrawString(kost + "", new Font("Arial", 12), Brushes.Black, new Point(300, i * 20 + 320));
              //  e.Graphics.DrawString(rad + "", new Font("Arial", 12), Brushes.Black, new Point(25, i * 20 + 320));
            }
             e.Graphics.DrawString(lblDash.Text, new Font("Arial", 12), Brushes.Black, new Point(25, 500));
             e.Graphics.DrawString("Total", new Font("Arial", 12), Brushes.Black, new Point(25, 550));
             e.Graphics.DrawString(sum.ToString(), new Font("Arial", 12), Brushes.Black, new Point(300, 550));
        }
        /*
         * Version 3
         * */
        private void btnTop_Click(object sender, EventArgs e)
        {
              IEnumerable<Produkt> produkts = (from p in listpro   
                                              orderby p.Name ascending
                                            
                                             select p).Take(2);
                BindingSource src = new BindingSource(produkts, null);              
                listBox3.DataSource = src;    
             
        }

       

       
    }
}
