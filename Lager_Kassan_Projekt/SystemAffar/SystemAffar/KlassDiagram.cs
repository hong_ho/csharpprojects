﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SystemAffar
{
    public partial class KlassDiagram : Form
    {
        public KlassDiagram()
        {
            InitializeComponent();
        }

        private void btnKassa_Click(object sender, EventArgs e)
        {
            Kassabruk k = new Kassabruk();
            k.Show();
            this.Hide();
        }

        private void btnLager_Click(object sender, EventArgs e)
        {
            Lagerarbete l = new Lagerarbete();
            l.Show();
            this.Hide();
        }

        private void btnHem_Click(object sender, EventArgs e)
        {
            Hem f = new Hem();
            f.Show();
            this.Hide();
        }
    }
}
