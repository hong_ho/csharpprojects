﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SystemAffar
{
    public partial class Hem : Form
    {
      
        public Hem()
        {
            InitializeComponent();         
        }

        private void btnLager_Click(object sender, EventArgs e)
        {
            Lagerarbete l = new Lagerarbete();
            l.Show();
            this.Hide();
        }

        private void btnKassa_Click(object sender, EventArgs e)
        {
            Kassabruk k = new Kassabruk();
            k.Show();
            this.Hide();
        }

        private void btnDiagram_Click(object sender, EventArgs e)
        {
            KlassDiagram kd = new KlassDiagram();
            kd.Show();
            this.Hide();
        }
    }
}
