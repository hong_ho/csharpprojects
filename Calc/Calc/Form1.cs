﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calc
{
    public partial class Form1 : Form
    {        
        string tal1, tal2, operators;
        decimal sum;
        bool flag;
        bool res1, res2;
        decimal nr1, nr2;
        public Form1()
        {
            InitializeComponent();    
        }       
        // dai dien cho all number-knappar 
        private void btnNumber_Click(object sender, EventArgs e)
        {
            Button knappar = (Button)sender;
            if (flag == true)
                txt1.Text = "";// nu bam vao nut operator thi tal1 nam giu so1 va xoa empty
            if(knappar.Text ==",")// khi la decimal
            {
                if(!txt1.Text.Contains(","))
                    txt1.Text += knappar.Text;
            }
            else
                txt1.Text += knappar.Text;// so bthuong ko decimal
            flag = false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            txt1.Text = "";
        }
        //Enter-knappen
        private void Enter_Click(object sender, EventArgs e)
        {
           
            tal2 = txt1.Text;
            res1 = decimal.TryParse(tal1, out nr1);
            res2 = decimal.TryParse(tal2, out nr2);
            if (res1 == true && res2 == true)
            {
                if (operators == "+")
                {
                    sum = nr1+ nr2;
                    txt1.Text = Convert.ToString(sum);
                }
                else if (operators == "-")
                {
                    sum = nr1 - nr2;
                    txt1.Text = Convert.ToString(sum);
                }
                else if (operators == "/")
                {
                    
                    if (nr2 == 0)
                    {
                        MessageBox.Show("Nämnare får inte vara 0!");
                    }
                    else
                    {
                        sum = nr1 / nr2;
                        txt1.Text = Convert.ToString(sum);
                    }
                }
                else
                {
                    sum = nr1 * nr2;
                    txt1.Text = Convert.ToString(sum);
                }
            }
            else
                MessageBox.Show("fel att konvertera!");
            
        }
        // dai dien cho knappar +-*%
        private void btnPlus_Click(object sender, EventArgs e)
        {
            Button btnOperator = (Button)sender;
            operators = btnOperator.Text;// ta text från operator knappar
           tal1 = txt1.Text;// lay so dau tien
           
            flag = true;
         }

        private void btnPlusMinus_Click(object sender, EventArgs e)
        {
            //neu da co dau -, neu ban vao knapp lan nua thi dau - bien mat
            if (txt1.Text.Contains("-"))
                txt1.Text=txt1.Text.Remove(0, 1);
            else// neu chua co dau - thi cong them dau - vao
                txt1.Text = "-" + txt1.Text;
        }
    }
}
